package it.unibg.so.lab3;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class CyclicBarrierExample {
	
	private static final int THREADS = 3;
    private CyclicBarrier barrier = new CyclicBarrier(THREADS);

    public static void main(String[] args) throws InterruptedException {
        CyclicBarrierExample instance = new CyclicBarrierExample();
        instance.init();
    }

    public void init() throws InterruptedException {
        for (int i=0; i<THREADS; ++i) {
            Thread th = new Thread(new MyWorkerThread(), "Worker" + i);
            th.start();
            Thread.sleep(1000);
        }
    }


    private class MyWorkerThread implements Runnable {
        @Override
        public void run() {
            System.out.println(Thread.currentThread().getName() + " started...");
            System.out.println(Thread.currentThread().getName() + " wait...");
            try {
                barrier.await();
            } catch (InterruptedException | BrokenBarrierException e) {
                e.printStackTrace();
            }
            
            System.out.println(Thread.currentThread().getName() + " running...");
        }
    }

}
