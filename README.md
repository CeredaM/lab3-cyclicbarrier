# CYCLIC BARRIER EXAMPLE #

Semplice esempio di utilizzo della classe `java.util.concurrent.CyclicBarrier`.
Il programma lancia 3 thread concorrenti che si attendono a vicenda utilizzando il meccanismo di sincronizzazione della barriera. Dopodiché vengono risvegliati e terminano l'esecuzione.